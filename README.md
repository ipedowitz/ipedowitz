## Ian Pedowitz's README

**Ian Pedowitz - Strategy and Operations (CTO)**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Connect with me

<a href="https://gitlab.com/ipedowitz"><img align="left" src="https://img.shields.io/badge/-GitLab-380D75?&style=plastic&logo=GitLab&logoColor=white" /></a>
<a href="https://www.linkedin.com/in/ijpedowitz/"><img align="left" src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=plastic&logo=LinkedIn&logoColor=white" /></a>
<a href="mailto:ipedowitz@gitlab.com"><img align="left" src="https://img.shields.io/badge/Email-EA4335?&style=plastic&logo=Gmail&logoColor=white" /></a>
<a href="https://calendly.com/gitlab-ianpedowitz/25min"><img align="left" src="https://img.shields.io/badge/Schedule a Meeting-4285F4?&style=plastic&logo=Google Calendar&logoColor=white" /></a>
<a href="https://forms.gle/5V3bRRgFzDK8TuA9A"><img align="left" src="https://img.shields.io/badge/-Feedback%20Form-6C4AB3?&style=plastic&logo=Google Drive&logoColor=white" /></a>
&nbsp;

## About me

1. I grew up in [Oceanside, NY](https://en.wikipedia.org/wiki/Oceanside,_New_York), went to undergrad at [Rensselaer Polytechnic Institute (RPI)](https://www.rpi.edu/) in [Troy, NY](https://en.wikipedia.org/wiki/Troy,_New_York), lived in [Norwalk, CT](https://en.wikipedia.org/wiki/Norwalk,_Connecticut) for a year, [Stamford, CT](https://en.wikipedia.org/wiki/Stamford,_Connecticut) for six months, [San Francisco, CA](https://en.wikipedia.org/wiki/San_Francisco) for 11 years, and now reside in [Eloy, AZ](https://en.wikipedia.org/wiki/Eloy,_Arizona) nearby [Skydive Arizona](https://www.skydiveaz.com/)
1. I'm a private pilot with over 250 hours flying Cessna 172/182, AMD Alarus, Piper Archer/Cherokee/Warrior, and Cirrus SR20/22
1. I'm a pro-am skydiver and train and compete at the National level in Formation Skydiving as well as participate in World level record events around the globe. I hold 16 National Medals or Awards and five World Records. I have over 5,300 skydives. See [Get Together & Learn - Those who don't jump will never fly](https://docs.google.com/presentation/d/1B7LzJd-b304IB_67xVfBT_k4aGCUYOBZlv41fuJRGAA) if you'd like to learn more about my hobby.
1. I've visited [25 countries](https://visitedplaces.com/view/?map=world&projection=geoNaturalEarth1&theme=dark-green&water=1&graticule=0&names=1&duration=2000&placeduration=100&slider=0&autoplay=0&autozoom=none&autostep=0&home=US&places=~AT_CZ_FR_IE_NL_PL_PT_CH_UA_GB_CA_CR_MX_NI_US_VI_CO_ID_IL_JO_AE_AU_NZ_GR_BE) and [39 states](https://visitedplaces.com/view/?map=usa&projection=geoAlbersUsa&theme=dark-green&water=0&graticule=0&names=1&duration=2000&placeduration=100&slider=0&autoplay=0&autozoom=none&autostep=0&home=US&places=~US-AL_US-AR_US-AZ_US-CA_US-CO_US-CT_US-DE_US-FL_US-GA_US-HI_US-IA_US-IL_US-LA_US-MA_US-MD_US-MO_US-MS_US-NC_US-NE_US-NH_US-NJ_US-NM_US-NV_US-NY_US-OH_US-OK_US-OR_US-PA_US-RI_US-SC_US-TN_US-TX_US-UT_US-VA_US-VT_US-WA_US-WI_US-INI_US-IN) in the US
1. I fully immerse myself in anything I'm passionate about
1. [GitLab's CREDIT values](https://about.gitlab.com/handbook/values/) (🤝 Collaboration, 📈 Results for Customers, ⏱️ Efficiency, 🌐 Diversity, Inclusion & Belonging, 👣 Iteration, and 👁️ Transparency) resonate deeply with me, and I feel I was practicing them before even formally joining as a team member. I'm extremely excited to be at a company where the culture is rooted in these values.

## How you can help me

1. I welcome feedback at any time and treat all forms of feedback whether positive, constructive, or negative; as a gift
1. If you work at GitLab and prefer to give feedback anonymously, you can use [this form](https://forms.gle/5V3bRRgFzDK8TuA9A). Getting feedback continuously rather than waiting for formal reviews helps me improve my ways of working and habits in smaller steps, bringing a level of iteration to personal improvement which is more manageable.
1. If you ever feel like I could be doing something differently to be more helpful to you, your team, or the company; please let me know

## My working style

1. I tend to think and work non-linearly so embrace working [asynchronously](https://about.gitlab.com/company/culture/all-remote/asynchronous/) and appreciate that GitLab [measures impact not activity](https://about.gitlab.com/handbook/values/#measure-impact-not-activity) and that [GitLab's Guide to All-Remote](https://about.gitlab.com/company/culture/all-remote/guide/#the-remote-manifesto) promotes `Flexible working hours (over set working hours)` as well as [non-linear workdays](https://about.gitlab.com/company/culture/all-remote/guide/#non-linear-workday) which I fully embrace
1. Most days I work from 8:30am to 5:30pm Mountain Standard Time (GMT-7). I prefer to take all meetings between 10am and 4pm Mountain Standard Time (GMT-7), but make exceptions when needed.
1. When inviting me to a meeting, please make it clear in the invite or meeting agenda why my participation is important. This helps me manage my time appropriately among competing priorities.
1. Please try to send meeting invites 2+ days ahead of when you'd like to meet, unless we've agreed to otherwise or it's very urgent. It helps me better plan my work and schedule ahead of time and allows me to review material in advance if needed.
1. I really value building strong relationships professionally and personally. My network and knowing who I can rely on and reach out to for what and when is one of my assets.
1. I'm a collaborative person who is just as happy contributing as a peer or leading initiatives. I like to do what's best for all involved and have [no ego](https://about.gitlab.com/handbook/values/#no-ego).
1. I value my time off work to recharge and aim to not check any work messages outside of my working hours

## What I assume about others

1. I truly believe we're all in this together and that everyones default is to be helpful
1. I assume no malice is ever intended when mistakes are made and believe that how one recovers from a mistake is more important than the mistake itself

## What I want to earn

1. I want to earn everyone's trust and respect and believe this is core to leadership
1. I want to earn the knowledge I lack to do my job effectively
1. I want the team to earn the results of the hard work we all do

## Communicating with me

1. I tend to be very friendly and cordial in communication and use emoji fairly frequently. I believe that in a world of text communication emoji can help provide those non-verbal cues we miss from video/in-person communication.
1. I will read and respond to all forms of communication and generally do not need reminders or pings.
   1. When I'm not on [PTO](https://about.gitlab.com/handbook/paid-time-off/) I strive to clear out any unread emails, Slacks, and GitLab To-Do's by the end of my working day as I follow the [inbox zero](https://www.techtarget.com/whatis/definition/inbox-zero) mentality. If for some reason I did not respond the same day, I will respond the next working day.
   1. When I'm returning from PTO I strive to be caught up and having responded to anything received within one business day for every week I was off
1. I try extremely hard to communicate in an inclusive manner and appreciate timely feedback if I did not
1. I'm extremely detail oriented and `precise`. If I ask several questions in a message; I expect all of them to have answers, an `I don't know`, or a redirect. I don't like to ask the same question multiple times.

## Strengths/Weaknesses

### Strengths

1. I'm very good at understanding nuanced details and reading in between the lines. I can look at a set of data or problem and see things not immediately apparent to others.
1. I care to the nth degree and will protect anything I'm passionate about
1. I'll say `I don't know` when I truly don't - I won't pretend to know something I don't or be an expert in an area where I'm not
1. I'll do whatever I can to help someone, or if I can't, I'll point them to someone who can help them better than I can
1. I won't propose solutions that end up `painting into a corner` - I truly believe in using [iteration](https://about.gitlab.com/handbook/values/#iteration) when solving problems which is one of GitLab's CREDIT [values](https://about.gitlab.com/handbook/values/)

### Weaknesses

1. I've burned out several times in my career - if you ever see behaviors where you believe I'm heading down that path again, please let me know
1. I can sometimes be fairly direct, but I try hard to be polite when being direct
1. While I feel I'm open minded, if I believe I know the best solution to a problem, I will usually try to defend it strongly. Feel free to challenge me if you have a better solution - I do believe that better solutions come from the intersection of multiple ideas from people of diverse backgrounds versus an individual mindset.
1. I have a hard time saying `no` which can result in me taking on too much. If you see me doing this, please tell me.

### Personality tests (by recency)

#### 2023 Kerage Style Indicator - Power

My [Kerage Style Indicator](https://www.keragecourses.com/style-indicator/) is `Power`.

>>>
Your Style Indicator results suggest that you tend to be a diligent, agreeable and dependable person. You
most likely enjoy the process of bringing a group together to get tangible results accomplished. You are always willing to take on more than your fair share, which can be a strength in your business performance but also a weakness when you allow yourself to become overextended.

Strengths in Business:

1. Very focused on tangible results
1. Sustaining relationships over time
1. Fairness toward others

Weaknesses in Business:

1. Can take on too much
1. Too tolerant of under performance
1. May lose sight of priorities

Pet Peeves:

1. Rude or abusive people
1. Being excluded from the process
1. Having the value you bring discounted
>>>

#### 2022 Gallup CliftonStrengths 34 - I lead with Strategic Thinking

My [CliftonStrengths](https://www.gallup.com/cliftonstrengths/en/252137/home.aspx) results are: Arranger, Analytical, Intellection, Achiever, Significance. I lead with [Strategic Thinking](https://www.gallup.com/cliftonstrengths/en/252080/strategic-thinking-domain.aspx). 

> *"People with dominant Strategic Thinking themes absorb and analyze information that informs better decisions. They know how to help individuals absorb and analyze information that can inform better decisions."*

#### 2022 16 Personalities - MBTI Consul Type

1. **Personality type:** [Consul (ESFJ-T)](https://www.16personalities.com/esfj-personality)
1. **Traits:** Extraverted – 56%, Observant – 64%, Feeling – 56%, Judging – 65%, Turbulent – 54%
1. **Role:** [Sentinel](https://www.16personalities.com/articles/roles-sentinels)
1. **Strategy:** [Social Engagement](https://www.16personalities.com/articles/strategies-social-engagement)
1. [Workplace Habits](https://www.16personalities.com/esfjs-at-work)
  > Teamwork is a concept that Consuls have no trouble putting into practice. Often seeking friends at work, people with this personality type are almost always willing to lend a hand when and where it’s needed. Excellent networkers, Consuls always seem to “know just the person” to bring a project together on time.

Note - I've previously tested both as an INTJ and an ENTJ. I consider myself a social introvert at the end of the day as I love group environments, but once I feel that my social tank is full, I need personal alone time to recharge.

## Statistics

<!-- See https://gitlab.com/oregand/gitlab-readme-stats -->

### GitLab Statistics

![GitLab Stats](https://gitlab-readme-stats.vercel.app/api?username=ipedowitz&show_icons=true&theme=dark)
